import math
from typing import List, Optional


class TrieNode :
    def __init__(self) :
        """
        Function description: Initializes a TrieNode with required attributes for Trie construction and search.

        Approach description: The TrieNode serves as a structural element in the Trie, housing potential child nodes,
        a word completion flag, and details about words (frequency, actual word, and definition) in its subtree.

        :Input:
        No input parameters for initialization.

        :Output, return or postcondition:
        An initialized TrieNode with:
        - children: A list to hold potential child nodes.
        - is_end_of_word: A flag indicating whether a node completes a word.
        - frequency: The frequency of the highest frequency word in its subtree.
        - word: The highest frequency word in its subtree.
        - definition: The definition of the highest frequency word.
        - prefix_count: Count of words that share the prefix leading to the node.

        :Time complexity:
        O(1), since initializing attributes to default values or empty data structures takes constant time.

        :Aux space complexity:
        O(1), as the space used does not grow with input size, maintaining a consistent utilization.
        """
        self.children = [None] * 26
        self.is_end_of_word = False
        self.frequency = 0
        self.word = ""
        self.definition = ""
        self.prefix_count = 0


class Trie :
    def __init__(self, Dictionary) :
        """
        Function description: Initializes a Trie and constructs it using the provided dictionary.

        Approach description: The Trie class facilitates the organized storage of words, their definitions,
        and frequencies in a way that enables efficient retrieval and search operations. The provided dictionary
        is utilized to build the Trie upon initialization.

        :Input:
        Dictionary: (list of tuples) A list where each tuple contains a word, its definition, and frequency.

        :Output, return or postcondition:
        An initialized and constructed Trie, wherein each word from the Dictionary is inserted in a manner
        that facilitates rapid prefix-based search operations.

        :Time complexity:
        O(T), where T is the total number of characters in all the words from the Dictionary. The Trie is constructed
        by iterating through every word and, subsequently, every character in each word, resulting in a time complexity
        that is linear with respect to the total character count.

        :Aux space complexity:
        O(T), where T is the total number of characters in all the words from the Dictionary, as a node may be
        instantiated for each character in the input words, making the auxiliary space complexity linear with respect to
        the total number of characters.
        """
        self.root = self.getNode()
        self.construct_trie(Dictionary)

    def getNode(self) :
        """
        Function description:
        This function generates and returns a new TrieNode object.

        Approach description:
        Instantiate a new TrieNode and return it.

        :Input:
        No input parameters.

        :Output, return or postcondition:
        Returns a new instance of TrieNode.

        :Time complexity:
        O(1), as the instantiation of a new TrieNode performs a set of constant-time operations.

        :Aux space complexity:
        O(1), since the function uses no auxiliary space that grows with input.
        """
        return TrieNode()

    def char_to_index(self, ch) :
        """
        Function description:
        Converts a character to its respective index in the children array of a TrieNode.

        Approach description: Subtracts the ASCII value of 'a' from the ASCII value of the input character to
        determine its alphabetical index.

        :Input:
        ch:
        A single character (a string of length 1) that is converted to its respective alphabetical index.

        :Output, return or postcondition:
        Returns an integer representing the alphabetical index of the character.

        :Time complexity:
        O(1), since the function performs a subtraction operation which takes constant time.

        :Aux space complexity:
        O(1), as it uses no auxiliary space.
        """
        return ord(ch) - ord('a')

    def insert(self, word, definition, frequency) :
        """
        Function description: This function inserts a new word along with its definition and frequency into the Trie.
        It updates the information (highest frequency word and its definition) at each node along the path if the
        inserting word has a higher frequency or has the same frequency but is lexicographically smaller.
        Additionally, it updates the prefix count of each node along the path which indicates how many words share
        the prefix up to that node.

        Approach description: We iterate through each character in the provided word, navigating through the Trie's
        nodes. For each character, we convert it to an index and find the corresponding child node. If no child node
        exists, we create a new one. Along the path of insertion, we update the prefix_count and check whether the
        inserting word should update the most frequent word information stored at each node based on its frequency
        and lexicographical order.

        :Input:
        word: (str) The word to be inserted into the Trie.
        definition: (str) The word's definition to be stored in the Trie.
        frequency: (int) The word's frequency to be stored in the Trie.

        :Output, return, or postcondition: Word, definition, and frequency are accurately inserted into the Trie,
        ensuring that all nodes reflect the most frequent word data in their respective subtrees.

        :Time complexity: O(L), where L is the length of the word being inserted. Each insertion operation (character
        check, node creation, and assignment) is O(1), but we perform these operations for each character in the word.

        :Aux space complexity: O(L), as a new TrieNode may be instantiated for each character in the word if no
        suitable path already exists in the Trie.
        """
        pCrawl = self.root
        length = len(word)

        # Update the root node if this word has higher frequency
        if frequency > pCrawl.frequency or \
                (frequency == pCrawl.frequency and word < pCrawl.word) :
            pCrawl.word = word
            pCrawl.definition = definition
            pCrawl.frequency = frequency

        # Increment the root's prefix_count since every word will be a descendant
        pCrawl.prefix_count += 1

        for level in range(length) :
            index = self.char_to_index(word[level])
            if not pCrawl.children[index] :
                pCrawl.children[index] = self.getNode()
            pCrawl = pCrawl.children[index]

            pCrawl.prefix_count += 1

            # Check if the provided word has a higher frequency or (if equal) is lexicographically smaller than the word
            # stored in the current Trie node.
            if pCrawl.frequency < frequency or \
                    (pCrawl.frequency == frequency and word < pCrawl.word) :
                pCrawl.frequency = frequency
                pCrawl.word = word
                pCrawl.definition = definition

        pCrawl.is_end_of_word = True

    def construct_trie(self, Dictionary) :
        """
        Function description: Constructs a Trie by inserting each word from the provided dictionary along with its
        respective definition and frequency.

        Approach description: Iterates through each entry in the provided dictionary and calls the insert() method to
        insert each word into the Trie.

        :Input: Dictionary: A list of lists, where each inner list contains a word (string), its definition (string),
        and its frequency (integer).

        :Output, return or postcondition: No explicit return value. The Trie structure is built/modified in place by
        inserting each word from the Dictionary.

        :Time complexity: O(T), where T is the total number of characters of all words in Dictionary.txt. This is
        because each insertion is O(L), where L is the length of the word being inserted, and we perform an insertion
        for each word, iterating through every character of all words.

        :Aux space complexity:
        O(T), since it uses extra auxiliary space that grows with input.
        """
        for entry in Dictionary :
            self.insert(entry[0], entry[1], entry[2])

    def prefix_search(self, prefix) :
        """
        Function description: This function searches for a prefix within the Trie and returns the most frequent word
        with its definition that matches the prefix, along with the count of words with the prefix. The function
        proficiently deals with diverse scenarios, correctly returning [None, None, 0] in instances where the prefix is
        non-existent within the Trie and returning the word details with the highest frequency in case the prefix is a
        blank string.

        Approach description: Starting at the root of the Trie, it looks at each character in the provided prefix one
        by one, moving through the Trie tree structure. If, while moving through, it finds that a character in the
        prefix doesn’t have a path (or node) in the Trie, it immediately knows that no words with that prefix exist,
        and returns a result indicating so ([None, None, 0]). If it successfully navigates through all characters in
        the prefix, it uses the node it lands on to find and return the highest frequency word with that prefix,
        its definition, and the count of words with that prefix. This is done efficiently, ensuring quick responses
        and minimal usage of additional memory.

        :Input:
        prefix: (str) The prefix for which to search in the Trie.

        :Output, return or postcondition: Returns a list containing the most frequent word with the prefix,
        its definition, and the count of words starting with the prefix. If no words start with the prefix,
        it returns [None, None, 0].

        :Time complexity: O(M + N), where M is the length of the prefix to be searched and N is the total number of
        characters in the word with the highest frequency and its definition. Traversal through the Trie is O(M) and
        string comparisons can be O(N) in worst-case scenarios (if we compare entire strings).

        :Aux space complexity: O(1), no additional data structures with a size dependent on the input are used,
        maintaining constant additional space usage regardless of input size.
        """
        # If the provided prefix is an empty string, return the most frequent word in the Trie (stored at the root), its
        # definition, and the count of words in the Trie (also stored at the root).
        if prefix == "" :
            return [self.root.word, self.root.definition, self.root.prefix_count]

        pCrawl = self.root
        length = len(prefix)

        # Iterate through each character in the provided prefix.
        for level in range(length) :
            index = self.char_to_index(prefix[level])  # Convert the current character to its respective index in the
            # children array.
            if not pCrawl.children[index] :
                return [None, None, 0]
            pCrawl = pCrawl.children[index]  # Move to the next node in the Trie.

        return [pCrawl.word, pCrawl.definition, pCrawl.prefix_count]


class TrieNode :
    def __init__(self) :
        """
        Function description: Initializes a TrieNode with required attributes for Trie construction and search.

        Approach description: The TrieNode serves as a structural element in the Trie, housing potential child nodes,
        a word completion flag, and details about words (frequency, actual word, and definition) in its subtree.

        :Input:
        No input parameters for initialization.

        :Output, return or postcondition:
        An initialized TrieNode with:
        - children: A list to hold potential child nodes.
        - is_end_of_word: A flag indicating whether a node completes a word.
        - frequency: The frequency of the highest frequency word in its subtree.
        - word: The highest frequency word in its subtree.
        - definition: The definition of the highest frequency word.
        - prefix_count: Count of words that share the prefix leading to the node.

        :Time complexity:
        O(1), since initializing attributes to default values or empty data structures takes constant time.

        :Aux space complexity:
        O(1), as the space used does not grow with input size, maintaining a consistent utilization.
        """
        self.children = [None] * 26
        self.is_end_of_word = False
        self.frequency = 0
        self.word = ""
        self.definition = ""
        self.prefix_count = 0


class Trie :
    def __init__(self, Dictionary) :
        """
        Function description: Initializes a Trie and constructs it using the provided dictionary.

        Approach description: The Trie class facilitates the organized storage of words, their definitions,
        and frequencies in a way that enables efficient retrieval and search operations. The provided dictionary
        is utilized to build the Trie upon initialization.

        :Input:
        Dictionary: (list of tuples) A list where each tuple contains a word, its definition, and frequency.

        :Output, return or postcondition:
        An initialized and constructed Trie, wherein each word from the Dictionary is inserted in a manner
        that facilitates rapid prefix-based search operations.

        :Time complexity:
        O(T), where T is the total number of characters in all the words from the Dictionary. The Trie is constructed
        by iterating through every word and, subsequently, every character in each word, resulting in a time complexity
        that is linear with respect to the total character count.

        :Aux space complexity:
        O(T), where T is the total number of characters in all the words from the Dictionary, as a node may be
        instantiated for each character in the input words, making the auxiliary space complexity linear with respect to
        the total number of characters.
        """
        self.root = self.getNode()
        self.construct_trie(Dictionary)

    def getNode(self) :
        """
        Function description:
        This function generates and returns a new TrieNode object.

        Approach description:
        Instantiate a new TrieNode and return it.

        :Input:
        No input parameters.

        :Output, return or postcondition:
        Returns a new instance of TrieNode.

        :Time complexity:
        O(1), as the instantiation of a new TrieNode performs a set of constant-time operations.

        :Aux space complexity:
        O(1), since the function uses no auxiliary space that grows with input.
        """
        return TrieNode()

    def char_to_index(self, ch) :
        """
        Function description:
        Converts a character to its respective index in the children array of a TrieNode.

        Approach description: Subtracts the ASCII value of 'a' from the ASCII value of the input character to
        determine its alphabetical index.

        :Input:
        ch:
        A single character (a string of length 1) that is converted to its respective alphabetical index.

        :Output, return or postcondition:
        Returns an integer representing the alphabetical index of the character.

        :Time complexity:
        O(1), since the function performs a subtraction operation which takes constant time.

        :Aux space complexity:
        O(1), as it uses no auxiliary space.
        """
        return ord(ch) - ord('a')

    def insert(self, word, definition, frequency) :
        """
        Function description: This function inserts a new word along with its definition and frequency into the Trie.
        It updates the information (highest frequency word and its definition) at each node along the path if the
        inserting word has a higher frequency or has the same frequency but is lexicographically smaller.
        Additionally, it updates the prefix count of each node along the path which indicates how many words share
        the prefix up to that node.

        Approach description: We iterate through each character in the provided word, navigating through the Trie's
        nodes. For each character, we convert it to an index and find the corresponding child node. If no child node
        exists, we create a new one. Along the path of insertion, we update the prefix_count and check whether the
        inserting word should update the most frequent word information stored at each node based on its frequency
        and lexicographical order.

        :Input:
        word: (str) The word to be inserted into the Trie.
        definition: (str) The word's definition to be stored in the Trie.
        frequency: (int) The word's frequency to be stored in the Trie.

        :Output, return, or postcondition: Word, definition, and frequency are accurately inserted into the Trie,
        ensuring that all nodes reflect the most frequent word data in their respective subtrees.

        :Time complexity: O(L), where L is the length of the word being inserted. Each insertion operation (character
        check, node creation, and assignment) is O(1), but we perform these operations for each character in the word.

        :Aux space complexity: O(L), as a new TrieNode may be instantiated for each character in the word if no
        suitable path already exists in the Trie.
        """
        pCrawl = self.root
        length = len(word)

        # Update the root node if this word has higher frequency
        if frequency > pCrawl.frequency or \
                (frequency == pCrawl.frequency and word < pCrawl.word) :
            pCrawl.word = word
            pCrawl.definition = definition
            pCrawl.frequency = frequency

        # Increment the root's prefix_count since every word will be a descendant
        pCrawl.prefix_count += 1

        for level in range(length) :
            index = self.char_to_index(word[level])
            if not pCrawl.children[index] :
                pCrawl.children[index] = self.getNode()
            pCrawl = pCrawl.children[index]

            pCrawl.prefix_count += 1

            # Check if the provided word has a higher frequency or (if equal) is lexicographically smaller than the word
            # stored in the current Trie node.
            if pCrawl.frequency < frequency or \
                    (pCrawl.frequency == frequency and word < pCrawl.word) :
                pCrawl.frequency = frequency
                pCrawl.word = word
                pCrawl.definition = definition

        pCrawl.is_end_of_word = True

    def construct_trie(self, Dictionary) :
        """
        Function description: Constructs a Trie by inserting each word from the provided dictionary along with its
        respective definition and frequency.

        Approach description: Iterates through each entry in the provided dictionary and calls the insert() method to
        insert each word into the Trie.

        :Input: Dictionary: A list of lists, where each inner list contains a word (string), its definition (string),
        and its frequency (integer).

        :Output, return or postcondition: No explicit return value. The Trie structure is built/modified in place by
        inserting each word from the Dictionary.

        :Time complexity: O(T), where T is the total number of characters of all words in Dictionary.txt. This is
        because each insertion is O(L), where L is the length of the word being inserted, and we perform an insertion
        for each word, iterating through every character of all words.

        :Aux space complexity:
        O(T), since it uses extra auxiliary space that grows with input.
        """
        for entry in Dictionary :
            self.insert(entry[0], entry[1], entry[2])

    def prefix_search(self, prefix) :
        """
        Function description: This function searches for a prefix within the Trie and returns the most frequent word
        with its definition that matches the prefix, along with the count of words with the prefix. The function
        proficiently deals with diverse scenarios, correctly returning [None, None, 0] in instances where the prefix is
        non-existent within the Trie and returning the word details with the highest frequency in case the prefix is a
        blank string.

        Approach description: Starting at the root of the Trie, it looks at each character in the provided prefix one
        by one, moving through the Trie tree structure. If, while moving through, it finds that a character in the
        prefix doesn’t have a path (or node) in the Trie, it immediately knows that no words with that prefix exist,
        and returns a result indicating so ([None, None, 0]). If it successfully navigates through all characters in
        the prefix, it uses the node it lands on to find and return the highest frequency word with that prefix,
        its definition, and the count of words with that prefix. This is done efficiently, ensuring quick responses
        and minimal usage of additional memory.

        :Input:
        prefix: (str) The prefix for which to search in the Trie.

        :Output, return or postcondition: Returns a list containing the most frequent word with the prefix,
        its definition, and the count of words starting with the prefix. If no words start with the prefix,
        it returns [None, None, 0].

        :Time complexity: O(M + N), where M is the length of the prefix to be searched and N is the total number of
        characters in the word with the highest frequency and its definition. Traversal through the Trie is O(M) and
        string comparisons can be O(N) in worst-case scenarios (if we compare entire strings).

        :Aux space complexity: O(1), no additional data structures with a size dependent on the input are used,
        maintaining constant additional space usage regardless of input size.
        """
        # If the provided prefix is an empty string, return the most frequent word in the Trie (stored at the root), its
        # definition, and the count of words in the Trie (also stored at the root).
        if prefix == "" :
            return [self.root.word, self.root.definition, self.root.prefix_count]

        pCrawl = self.root
        length = len(prefix)

        # Iterate through each character in the provided prefix.
        for level in range(length) :
            index = self.char_to_index(prefix[level])  # Convert the current character to its respective index in the
            # children array.
            if not pCrawl.children[index] :
                return [None, None, 0]
            pCrawl = pCrawl.children[index]  # Move to the next node in the Trie.

        return [pCrawl.word, pCrawl.definition, pCrawl.prefix_count]


def validate_inputs(preferences: List[List[int]], licences: List[int]) -> bool :
    """
    Function description: Check if the inputs (preferences and licences) are valid and non-empty and that no
    preference list is empty.

    Approach description: It checks for the non-emptiness of preferences and licences and confirms no inner list within
    preferences is empty by looping through them, providing a quick validity check of inputs.

    :Input:
    preferences: A list of lists containing integer preferences.
    licences: A list of integers indicating which persons have a driver's license.

    :Output, return or postcondition: A boolean indicating whether the inputs are valid.

    :Time complexity: O(n), where n is the number of persons (size of preferences list).

    :Aux space complexity: O(1)
    """
    return bool(preferences) and bool(licences) and not any(len(p) == 0 for p in preferences)  # O(n) time complexity


def initialize_allocation(n: int, licences: List[int]) -> (List[List[int]], List[int]) :
    """
    Function description: Initialize the allocation, unallocated persons, and drivers list, using the input
    size n and a copy of the licences list.

    Approach description: Given the total number of persons n and a list licences, it determines the number of cars d
    and returns three lists: an empty allocation structure (a list of empty lists), a list of all persons as
    initially unallocated, and a copy of licences.

    :Input:
    n: The number of persons to be allocated.
    licences: A list of integers indicating which persons have a driver's license.

    :Output, return or postcondition: A tuple containing an initial empty allocation list, a list of all
    unallocated persons, and a copy of the drivers' list.

    :Time complexity: O(n), where n is the number of persons to be allocated.

    :Aux space complexity: O(n), where n is the number of persons to be allocated.
    """
    d = math.ceil(n / 5)  # Number of destinations/cars
    return [[] for _ in range(d)], list(range(n)), licences.copy()  # O(n) time complexity


def allocate_drivers(allocation: List[List[int]], unallocated: List[int], drivers: List[int],
                     preferences: List[List[int]], d: int) -> bool :
    """
    Function description: Allocate at least two drivers to each car while considering their preferences.

    Approach description: It iterates through each vehicle and driver, trying to allocate at least two drivers per
    vehicle based on their preferences, updating allocation, unallocated, and drivers in the process. If it can't
    assign two drivers to a car, it returns False.

    :Input:
    allocation: A list of lists representing the current allocation of persons to cars.
    unallocated: A list of persons yet to be allocated.
    drivers: A list of persons with a driving license.
    preferences: A list of lists containing the preferences of each person.
    d: The total number of destinations/cars.

    :Output, return or postcondition: A boolean indicating whether at least two drivers could be allocated
    to each car.

    :Time complexity: O(d * l * n), where d is the number of destinations/cars, l is the number of licences,
    and n is the number of persons. In the worst case, this part could iterate over all drivers for all cars.

    :Aux space complexity: O(1)
    """
    for car in range(d) :  # O(n) time complexity
        allocated_drivers = 0
        for i in drivers[:] :  # Using slicing to avoid modifying the iterating list directly: O(n) time complexity
            if allocated_drivers >= 2 :  # Once two drivers are allocated, move to the next car.
                break
            if car in preferences[i] and i in unallocated :  # O(n) time complexity
                allocation[car].append(i)
                unallocated.remove(i)  # O(n) time complexity
                drivers.remove(i)  # O(n) time complexity
                allocated_drivers += 1
        if allocated_drivers < 2 :  # Cannot ensure two drivers for a car
            return False
    return True


def allocate_remaining(allocation: List[List[int]], unallocated: List[int], preferences: List[List[int]]) -> bool :
    """
    Function description: Allocate the remaining persons according to their preferences, ensuring no car is
    over-occupied.

    Approach description: For each unallocated person, it iterates through their preferences, trying to allocate them
    to a car if it doesn’t exceed the maximum occupancy (5). If a person cannot be allocated, the function returns
    False.

    :Input:
    allocation: A list of lists representing the current allocation of persons to cars.
    unallocated: A list of persons yet to be allocated. preferences: A list of lists containing the preferences of
    each person.

    :Output, return or postcondition: A boolean indicating whether all remaining persons could be allocated according to
     their preferences.

    :Time complexity: O(u * p), where u is the number of unallocated persons and p is the max length of a preference
    list.

    :Aux space complexity: O(1)
    """
    for i in unallocated[:] :  # To safely modify unallocated during iteration: O(n) time complexity
        allocated = False
        for dest in preferences[i] :  # O(n) time complexity
            if len(allocation[dest]) < 5 :  # Ensure the car is not fully booked.
                allocation[dest].append(i)
                unallocated.remove(i)  # O(n) time complexity
                allocated = True
                break
        if not allocated :  # If a person could not be allocated to any preferred destination.
            return False
    return True


def validate_final_allocation(allocation: List[List[int]], licences: List[int]) -> bool :
    """
    Function description: Validate the final allocations, ensuring each car has a valid number of occupants and at least
    one licensed driver.

    Approach description: It iterates through each car in allocation, checking if the number of persons is between 2 and
    5 and if there’s at least one licensed driver, ensuring the solution adheres to problem constraints.

    :Input:
    allocation: A list of lists representing the current allocation of persons to cars.
    licences: A list of integers indicating which persons have a driver's license.

    :Output, return or postcondition: A boolean indicating whether the final allocation is valid.

    :Time complexity: O(d * m), where d is the number of destinations/cars and m is the max number of members in a car.

    :Aux space complexity: O(1)
    """
    for car in allocation :  # O(n) time complexity
        if len(car) < 2 or len(car) > 5 :  # Validate car occupation
            return False
        if not any(i in licences for i in car) :  # Ensure at least one licensed driver
            return False
    return True


def allocate(preferences: List[List[int]], licences: List[int]) -> Optional[List[List[int]]] :
    """
    Function description: The 'allocate' function assigns individuals to vehicles, adhering to specific rules and
    preferences, and ensuring that each vehicle has a minimum of two drivers. It navigates through several stages:
    initializing allocation based on provided preferences and licenses, allocating at least two drivers per vehicle,
    assigning the remaining individuals according to their preferences, and validating the final allocation to make
    sure all the requirements (like car occupancy and licensed driver presence) are met.

    Approach description:  The function employs a greedy approach. Initially, the function validates inputs and sets up
    basic allocations while ensuring every vehicle has at least two drivers, prioritizing their preferences. After
    driver allocation, it attempts to assign the remaining individuals according to their respective preferences while
    maintaining a cap on vehicle occupancy. Post allocation, a final validation checks if each vehicle has between 2 and
    5 individuals and at least one licensed driver, ensuring the devised solution adheres to the stipulated constraints.
    If any step fails, the function promptly returns None; otherwise, it outputs the achieved allocation.

    :Input:
    preferences: A list of lists containing the preferences of each person.
    licences: A list of integers indicating which persons have a driver's license.

    :Output, return or postcondition: A list of lists representing the final allocation if possible, otherwise None.

    :Time complexity: The total worst case time complexity equals to O(n + d x l x n + u x p + d x m)
    We know that:
    d is the number of destinations/cars, since there are ⌈n/5⌉ cars and destinations, and thus d = ⌈n/5⌉ at maximum,
    simplifying to O(n) in the worst case.
    l is the length of 'licences' and in the worst case, every person has a license so l = n.
    u is the number of unallocated persons, since the worst-case scenario here is that no one is allocated initially, so
    u is bounded by n.
    p is the max length of a preference list, which is bounded by d, as persons will indicate their preferred
    destinations, which are numbered 0 through ⌈n/5⌉ - 1. So, everyone could have a preference for all available
    destinations. Thus, p is bounded by d, which is ⌈n/5⌉.
    m is the max number of members in a car, which is a constant number (5) and does not grow with n, so we can
    disregard it in big O notation.
    Substituting these back in: O(n + (n x n x n) + (n x n) + n) = O(n^3 + n^2+ 2n) = O(n^3), where n is the number of
    persons.

    :Aux space complexity: O(n), where n is the number of persons.
    """
    n = len(preferences)

    if not validate_inputs(preferences, licences) :
        return None

    allocation, unallocated, drivers = initialize_allocation(n, licences)
    d = math.ceil(n / 5)

    if not allocate_drivers(allocation, unallocated, drivers, preferences, d) :
        return None

    if not allocate_remaining(allocation, unallocated, preferences) :
        return None

    if not validate_final_allocation(allocation, licences) :
        return None

    return allocation
