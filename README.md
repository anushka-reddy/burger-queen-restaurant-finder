### **Burger Queen Restaurant Finder**

Welcome to the Burger Queen Restaurant Finder! This Python function is designed to assist Burger Queen, a popular chain of fast food restaurants, in selecting optimal sites to open restaurants along a newly built freeway. The goal is to maximize the overall revenue while ensuring that no two restaurants are within a specified distance of each other.

### Function Description

The function `restaurantFinder(d, site_list)` takes two arguments:

- `d`: The distance parameter representing the maximum distance allowed between selected restaurant sites.
- `site_list`: A list of size N containing the annual revenue (in million dollars) for each potential site along the freeway.

The function returns a tuple `(total_revenue, selected_sites)`, where:

- `total_revenue`: The total annual revenue if the company opens restaurants at the sites in `selected_sites`.
- `selected_sites`: A list containing the site numbers where the company should open their restaurants to maximize revenue. The list `selected_sites` contains site numbers in ascending order.

### Constraints

- The distance parameter `d` is a non-negative integer.
- The `site_list` always contains at least 1 site.
- The function must solve the problem using O(N) space and O(N) time in the worst-case scenario.

### Example Usage

Below are examples of how to use the `restaurantFinder` function:

```python
>>> restaurantFinder(1, [50, 10, 12, 65, 40, 95, 100, 12, 20, 30])
(252, [1, 4, 6, 8, 10])

>>> restaurantFinder(2, [50, 10, 12, 65, 40, 95, 100, 12, 20, 30])
(245, [1, 4, 7, 10])

>>> restaurantFinder(3, [50, 10, 12, 65, 40, 95, 100, 12, 20, 30])
(175, [1, 6, 10])

>>> restaurantFinder(7, [50, 10, 12, 65, 40, 95, 100, 12, 20, 30])
(100, [7])

>>> restaurantFinder(0, [50, 10, 12, 65, 40, 95, 100, 12, 20, 30])
(434, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
```

### Note

- If there are multiple possible answers (i.e., multiple ways to achieve the maximum revenue), the function can return any of the valid solutions.

For further inquiries or assistance, please contact the developer.

*Developer: Anushka Reddy*
*Contact: anushkar614@gmail.com*
